// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

#include "iohelper.hpp"
#include <api_scilab.h>
#include "Scierror.h"

/* ==================================================================== */

//
// name = getGraphName(g)
// gets the graph name
// You can use this for creating a Metanet graph
//
extern "C" int sci_getGraphName (char *fname)
{
    /* check that we have 1 parameters input */
    CheckRhs(1,1) ;

    /* check that we have 1 parameters output */
    CheckLhs(1,1) ;

    BglGraph* graph = (BglGraph*)(readPointerArgument(1));
    if (graph == NULL) {
        Scierror(999, "Can't retrieve graph");
        return 0;
    }

    string name = graph->getName();
	char* buffer = new char[name.length()+1];
	if (buffer == NULL) {
		Scierror(999, "No memory");
		return 0;
	}

	strcpy(buffer, name.c_str());
    SciErr sciErr = createMatrixOfString(pvApiCtx, Rhs+1, 1, 1, &buffer);
    BGL_ERROR_VOID
    delete buffer;
	LhsVar(1) = Rhs + 1;

	return 0;
}
/* ==================================================================== */
