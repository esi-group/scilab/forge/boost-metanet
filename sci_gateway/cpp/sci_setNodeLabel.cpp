// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

/* ==================================================================== */
#include "iohelper.hpp"
#include "Scierror.h"
/* ==================================================================== */

//
// setNodeLabel(myGraph, nodeLabel OR nodeIndex or Vertex, string newLabel)
// This function finds a vertex within a graph, and sets its label
//
extern "C" int sci_setNodeLabel (char *fname)
{
    BglGraph* graph;

    /* check that we have 3 parameters input */
    CheckRhs(3,3) ;

    /* check that we have 0-1 parameters output */
    CheckLhs(0,1) ;


    graph = (BglGraph*)(readPointerArgument(1));
    if (graph == NULL) {
        Scierror(999, "Can't retrieve graph");
        return 0;
    }
    Vertex node = getNodeFromGraph(graph, 2);
    string newLabel = readStringArgument(3);
    graph->setNodeLabel(node, newLabel);
    return 0;
}
/* ==================================================================== */
