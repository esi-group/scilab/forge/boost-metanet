// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

#include "iohelper.hpp"
#include <api_scilab.h>
#include "Scierror.h"

/* ==================================================================== */

//
// removeGraph(graph_name)
// removes a graph with the specified name from the Scilab memory
// After calling this function you can reuse the graph name,
// and the removed graph is no longer available
//
extern "C" int sci_removeGraph (char *fname)
{
	/* check that we have 1 parameters input */
	CheckRhs(1,1) ;

	/* check that we have from 0 to 1 parameters output */
	CheckLhs(0,1) ;

	string name = readStringArgument(1);
	BglGraph::RemoveGraph(name);
}
/* ==================================================================== */
