// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

#include "iohelper.hpp"
#include <api_scilab.h>
#include "Scierror.h"

/* ==================================================================== */

//
// [distances predecessors] = dijkstraSP(graph)
// performs a dijsktra shortest path algorithm on the graph
// The function returns :
// distances: the array of distances of each node from the first one
// predecessors: parents of each node. 
// For the starting node, the parent is the node itself
//

extern "C" int sci_dijkstraSP (char *fname)
{
    /* check that we have 2 parameters input */
    CheckRhs(2,2) ;

    /* check that we have from 3 parameters output */
    CheckLhs(2,2) ;

    BglGraph* graph = (BglGraph*)(readPointerArgument(1));
    if (graph == NULL) {
        Scierror(999, "Can't retrieve graph");
        return 0;
    }
    Vertex node = getNodeFromGraph(graph, 2);
    if (node == INVALID_VERTEX) {
    	Scierror(999, "Node nout found");
    	return 0;
    }
	size_t num_nodes = graph->getNumVertices();
	vector<double> distances(num_nodes);
	vector<Vertex> parents(num_nodes);
	graph->dijkstra(node, distances, parents);

	writeDoubleArray(distances, Rhs + 1);
	writeUnsignedIntArray(parents, Rhs + 2);

	LhsVar(1) = Rhs + 1;
	LhsVar(2) = Rhs + 2;

	return 0;
}
/* ==================================================================== */
