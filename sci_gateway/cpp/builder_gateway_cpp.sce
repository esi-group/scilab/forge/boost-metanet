// ====================================================================
// Allan CORNET
// DIGITEO 2008
// This file is released into the public domain
// ====================================================================

gateway_path = get_absolute_file_path('builder_gateway_cpp.sce');

libname = 'bglgraphgateway';
namelist = [
  'makeGraph' 'sci_makeGraph'
  'addNode' 'sci_addNode'
  'addEdge' 'sci_addEdge'
  'printGraph' 'sci_printGraph'
  'printVertex' 'sci_printVertex'
  'getGraphName' 'sci_getGraphName'
  'getNodeNumber' 'sci_getNodeNumber'
  'getEdgeNumber' 'sci_getEdgeNumber'
  'getNode' 'sci_getNode'
  'getHeadNodes' 'sci_getHeadNodes'
  'getTailNodes' 'sci_getTailNodes'
  'getAllNodeLabels' 'sci_getAllNodeLabels'
  'getAllEdgeLabels' 'sci_getAllEdgeLabels'
  'getAllEdgeWeights' 'sci_getAllEdgeWeights'
  'setNodeLabel' 'sci_setNodeLabel'
  'setEdgeLabel' 'sci_setEdgeLabel'
  'setEdgeWeight' 'sci_setEdgeWeight'
  'getGraph' 'sci_getGraph'
  'removeGraph' 'sci_removeGraph'
  'clearAllGraphs' 'sci_clearAllGraphs'
  'breadthFirstSearch' 'sci_breadthFirstSearch'
  'depthFirstSearch' 'sci_depthFirstSearch'
  'dijkstraSP' 'sci_dijkstraSP'
  'bellmanFordSP' 'sci_bellmanFordSP'
   ];
files = [
  'sci_makeGraph.cpp'  
  'sci_addNode.cpp'
  'sci_addEdge.cpp'
  'sci_printGraph.cpp'
  'sci_printVertex.cpp'
  'sci_getGraphName.cpp'
  'sci_getNodeNumber'
  'sci_getEdgeNumber'
  'sci_getNode.cpp'
  'sci_getHeadNodes'
  'sci_getTailNodes'
  'sci_getAllNodeLabels'
  'sci_getAllEdgeLabels'
  'sci_getAllEdgeWeights'
  'sci_setNodeLabel.cpp'
  'sci_setEdgeLabel.cpp'
  'sci_setEdgeWeight.cpp'
  'sci_getGraph.cpp'
  'sci_removeGraph.cpp'
  'sci_clearAllGraphs.cpp'
  'sci_breadthFirstSearch.cpp' 
  'sci_depthFirstSearch.cpp'
  'sci_dijkstraSP.cpp'
  'sci_bellmanFordSP.cpp'
  
  
  ];
ldflags = ''

if MSDOS then
  include1 = '../../src/bglgraph/includes';
  include2 = '../../src/iohelper/includes';
  cflags = '-I""'+include1+'""' + ' -I""'+include2+'""';
  libs = [...
  '../../src/bglgraph/src/libbglgraph',
  '../../src/iohelper/src/libiohelper',  
  ];
else
  include1 = gateway_path+'/../../src/bglgraph/includes';
  include2 = gateway_path;
  include3 = gateway_path+'/../../src/iohelper/includes';
  
cflags = '-I""'+include1+'"" -I""'+include2+'""' + ' -I""'+include3+'""';
  
  libs = [...
  '../../src/bglgraph/src/libbglgraph',
  '../../src/iohelper/src/libiohelper' 
  ];
end

tbx_build_gateway(libname, namelist, files, gateway_path, libs , ldflags, cflags);

clear tbx_build_gateway;
