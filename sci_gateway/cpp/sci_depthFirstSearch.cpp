// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

#include "iohelper.hpp"
#include <api_scilab.h>
#include "Scierror.h"

/* ==================================================================== */

//
// [distances discovery_times finish_times predecessors] = depthFirstSearch(graph)
// performs depth first search on the graph
// The function returns :
// distances: the array of distances of each node from the first one
// discovery_times: array of discovery times of each node
// finish_times: array of finish times of each node
// predecessors: parents of each node. 
// For the starting node, the parent is the node itself
//
extern "C" int sci_depthFirstSearch (char *fname)
{
    /* check that we have 1 parameters input */
    CheckRhs(1,1) ;

    /* check that we have 4 parameters output */
    CheckLhs(4,4) ;

    BglGraph* graph = (BglGraph*)(readPointerArgument(1));
    if (graph == NULL) {
        Scierror(999, "Can't retrieve graph");
        return 0;
    }

    size_t num_nodes = graph->getNumVertices();
	vector<	double > distances(num_nodes);
	vector<Size> discovery_times(num_nodes);
	vector<Size> finish_times(num_nodes);
	vector<Vertex> parents(num_nodes);
	graph->dfs(distances, discovery_times, finish_times, parents);
	writeDoubleArray(distances, Rhs + 1);
	writeUnsignedIntArray(discovery_times, Rhs + 2);
	writeUnsignedIntArray(finish_times, Rhs + 3);
	writeUnsignedIntArray(parents, Rhs + 4);
	LhsVar(1) = Rhs + 1;
	LhsVar(2) = Rhs + 2;
	LhsVar(3) = Rhs + 3;
	LhsVar(4) = Rhs + 4;

	return 0;
}
/* ==================================================================== */
