// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

/* ==================================================================== */
#include "iohelper.hpp"
#include "Scierror.h"
/* ==================================================================== */

//
// setEdgeWeight(myGraph, edgeWeight OR edgeIndex , string newWeight)
// This function finds an edge within a graph, and sets its weight
//
extern "C" int sci_setEdgeWeight (char *fname)
{
    BglGraph* graph;

    /* check that we have 3 parameters input */
    CheckRhs(3,4) ;

    /* check that we have 0-1 parameters output */
    CheckLhs(0,1) ;


    graph = (BglGraph*)(readPointerArgument(1));
    if (graph == NULL) {
        Scierror(999, "Can't retrieve graph");
        return 0;
    }
    Edge edge = getEdgeFromGraph(graph, 2);
    double newWeight = readDoubleArgument(Rhs);
    cout << "Rhs = " << Rhs << endl;
    graph->setEdgeWeight(edge, newWeight);
    return 0;
}
/* ==================================================================== */
