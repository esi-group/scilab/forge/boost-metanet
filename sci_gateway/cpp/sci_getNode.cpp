// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

#include <string>
#include <iostream>
#include "api_scilab.h"
#include "iohelper.hpp"
#include "Scierror.h"

/* ==================================================================== */
//
// getNode (BglGraph graph, string label) Gets a node with the given label
// or getNode (BglGraph graph, int index ) Gets a node with the given index
//

extern "C" int sci_getNode (char *fname)
{
    CheckLhs(0,1);
    CheckRhs(2,2);
    Vertex node;
    BglGraph* graph = (BglGraph*)readPointerArgument(1);
    if (graph == NULL) {
        Scierror(999, "error retrieving graph!");
        return 0	;
    }
    node = getNodeFromGraph(graph, 2);
    if (node == INVALID_VERTEX) {
        Scierror(999, "Failed to look up node");
        return 0;
    }

#if DEBUG
    cerr << "Printing node to cerr: " << endl;
    graph->printVertex(node, cerr);
#endif
    createPointer(pvApiCtx, Rhs + 1, (void*)node);
	LhsVar(1) = Rhs + 1;

    return 0;
}
/* ==================================================================== */
