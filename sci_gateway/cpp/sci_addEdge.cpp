// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

#include "iohelper.hpp"
#include "Scierror.h"

/* ==================================================================== */
//
// addEdge (graph, node1, node2, weight, label )
// adds an edge between node1 and node2 and optionally sets the weight and label
//
extern "C" int sci_addEdge (char *fname)
{
	CheckLhs(0,1);
	CheckRhs(3,5);

	Vertex node1, node2;
	BglGraph* graph = (BglGraph*)readPointerArgument(1);
	if (graph == NULL) {
		Scierror(999, "Error retrieving graph!");
		return 0;
	}

	node1 = getNodeFromGraph(graph, 2);
	node2 = getNodeFromGraph(graph, 3);
	double weight;
	switch (Rhs) {
        case 3:
            graph->addEdge(node1, node2);
            break;
        case 4:
            weight = readDoubleArgument(4);
            graph->addEdge(node1, node2, weight);
            break;
        case 5:
            weight = readDoubleArgument(4);
            string label = readStringArgument(5);
            graph->addEdge(node1, node2, weight, label);
            break;
	}

	return 0;
}
/* ==================================================================== */
