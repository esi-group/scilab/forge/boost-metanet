// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

#include "iohelper.hpp"
#include <api_scilab.h>
#include "Scierror.h"

/* ==================================================================== */

//
// [vertices] = getHeadNodes(graph)
// gets the Head Nodes for forming a Metanet graph
//
extern "C" int sci_getHeadNodes (char *fname)
{
    /* check that we have 1 parameters input */
    CheckRhs(1,1) ;

    /* check that we have 1 parameters output */
    CheckLhs(1,1) ;

    BglGraph* graph = (BglGraph*)(readPointerArgument(1));
    if (graph == NULL) {
        Scierror(999, "Can't retrieve graph");
        return 0;
    }

    size_t num_nodes = graph->getNumVertices();
    vector<Vertex> nodes(num_nodes);
	
    nodes = graph->getHeadNodes();
	writeToUnsignedIntArray<Vertex>(nodes, Rhs + 1);
	LhsVar(1) = Rhs + 1;

	return 0;
}
/* ==================================================================== */
