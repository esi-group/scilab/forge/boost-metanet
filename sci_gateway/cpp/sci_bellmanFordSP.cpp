// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

#include "iohelper.hpp"
#include <api_scilab.h>
#include "Scierror.h"

/* ==================================================================== */

//
// [distances predecessors] = bellmanFordSP(graph)
// performs a Bellman - Ford shortest path algorithm on the graph
//
extern "C" int sci_bellmanFordSP(char *fname)
{
    /* check that we have 2 parameters input */
    CheckRhs(2,2) ;

    /* check that we have from 3 parameters output */
    CheckLhs(2,2) ;

    BglGraph* graph = (BglGraph*)(readPointerArgument(1));
    if (graph == NULL) {
        Scierror(999, "Can't retrieve graph");
        return 0;
    }
    Vertex node = getNodeFromGraph(graph, 2);
	size_t num_nodes = graph->getNumVertices();
	vector<double> distances(num_nodes);
	vector<Vertex> parents(num_nodes);
	graph->bellman_ford(node, distances, parents);

	writeDoubleArray(distances, Rhs + 1);
	writeUnsignedIntArray(parents, Rhs + 2);

	LhsVar(1) = Rhs + 1;
	LhsVar(2) = Rhs + 2;

	return 0;
}
/* ==================================================================== */
