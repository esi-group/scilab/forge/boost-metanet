// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

#include "iohelper.hpp"
#include <api_scilab.h>
#include "Scierror.h"

/* ==================================================================== */

//
// clearAllGraphs() cleans all created graphs from the memory
// After this call, you won't be able to retrieve previously created 
// graphs with getGraphByName function
//
extern "C" int sci_clearAllGraphs (char *fname)
{
    // No input or output arguments expected
    CheckRhs(0,0) ;
    CheckLhs(0,0) ;

    BglGraph::ClearAllGraphs();
	return 0;
}
/* ==================================================================== */
