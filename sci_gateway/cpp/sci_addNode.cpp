// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================


#include "iohelper.hpp"
#include "Scierror.h"

/* ==================================================================== */
//
// addNode (Graph graph, string label)
// adds a node to the graph with a certain label
//
extern "C" int sci_addNode (char *fname)
{
	BglGraph * graph;
	string name;

	/* check that we have 2 parameters input */
	CheckRhs(2,2) ;

	/* check that we have from 0 to 1 parameters output */
	CheckLhs(0,1) ;

	//Get the graph pointer
	graph = (BglGraph*)readPointerArgument(1);
	if (graph == NULL) {
		Scierror(999, "Error retrieving graph");
		return 0;
	}

	graph->addNode(readStringArgument(2));
	return 0;
}
/* ==================================================================== */
