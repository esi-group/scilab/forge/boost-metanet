// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

#include "iohelper.hpp"
#include <api_scilab.h>
#include "Scierror.h"

/* ==================================================================== */

//
// myGraph = getGraph(graph_name)
// gets a graph with the specified name
//
extern "C" int sci_getGraph (char *fname)
{
    /* check that we have 1 parameters input */
    CheckRhs(1,1) ;

    /* check that we have from 0 to 1 parameters output */
    CheckLhs(0,1) ;
	BglGraph* graph;
	string name = readStringArgument(1);
	graph = BglGraph::GetGraph(name);
	if (graph == NULL) {
		Scierror(999, "Graph %s not found",name.c_str());
		return 0;
}
	createPointer(pvApiCtx, Rhs + 1, (void*)graph);
	LhsVar(1) = Rhs + 1;
	return 0;
}
/* ==================================================================== */
