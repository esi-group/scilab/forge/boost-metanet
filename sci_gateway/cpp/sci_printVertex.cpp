// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

#include <string>
#include <iostream>
#include "sciprint.h"

#define DEBUG 1
/* ==================================================================== */

#include "iohelper.hpp"

#include "stack-c.h"
#include "Scierror.h"
/* ==================================================================== */

//
// printVertex (BglGraph g, string label OR int nodeIndex)
// prints a vertex with the given label, or index
//
extern "C" int sci_printVertex (char *fname)
{
    BglGraph* graph;

    /* check that we have 2 parameters input */
    CheckRhs(2,2) ;

    /* check that we have from 0 to 1 parameters output */
    CheckLhs(0,1) ;


    graph = (BglGraph*)(readPointerArgument(1));
    if (graph == NULL) {
        Scierror(999, "Can't retrieve graph");
        return 0;
    }
#ifdef DEBUG
    sciprint("printing a vertex to sciprint\n");
#endif
    Vertex node = getNodeFromGraph(graph, 2);
    graph->printVertex(node, cout);
    return 0;
}
/* ==================================================================== */
