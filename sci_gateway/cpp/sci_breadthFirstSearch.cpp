// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

#include "iohelper.hpp"
#include <api_scilab.h>
#include "Scierror.h"

/* ==================================================================== */

//
// [distances discovery_times predecessors] = breadthFirstSearch(graph, start_vertex)
// performs breadthFirstSearch on the graph, starting from vertex start_vertex
//
extern "C" int sci_breadthFirstSearch (char *fname)
{
    /* check that we have 2 parameters input */
    CheckRhs(2,2) ;

    /* check that we have from 3 parameters output */
    CheckLhs(3,3) ;

    BglGraph* graph = (BglGraph*)(readPointerArgument(1));
    if (graph == NULL) {
        Scierror(999, "Can't retrieve graph");
        return 0;
    }
    Vertex node = getNodeFromGraph(graph, 2);
	size_t num_nodes = graph->getNumVertices();
	vector<double> distances(num_nodes);
	vector<Size> times(num_nodes);
	vector<Vertex> parents(num_nodes);
	graph->bfs(node, distances, times, parents);
	writeDoubleArray(distances, Rhs + 1);
	writeUnsignedIntArray(times, Rhs + 2);
	writeUnsignedIntArray(parents, Rhs + 3); // trying!
	LhsVar(1) = Rhs + 1;
	LhsVar(2) = Rhs + 2;
	LhsVar(3) = Rhs + 3;

	return 0;
}
/* ==================================================================== */
