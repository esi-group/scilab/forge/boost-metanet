// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

#include "iohelper.hpp"
#include <api_scilab.h>
#include "Scierror.h"

/* ==================================================================== */

//
// [labels] = getAllEdgeLabels(graph)
// gets labels for all the edges
// Use this for retrieving all edge labels to make a Metanet graph
//
extern "C" int sci_getAllEdgeLabels (char *fname)
{
    /* check that we have 1 parameters input */
    CheckRhs(1,1) ;

    /* check that we have 1 parameters output */
    CheckLhs(1,1) ;

    BglGraph* graph = (BglGraph*)(readPointerArgument(1));
    if (graph == NULL) {
        Scierror(999, "Can't retrieve graph");
        return 0;
    }

    size_t num_edges = graph->getNumEdges();
    vector<string> labels(num_edges);
	
    labels = graph->getAllEdgeLabels();
	writeStringArray(labels, Rhs + 1);
	LhsVar(1) = Rhs + 1;

	return 0;
}
/* ==================================================================== */
