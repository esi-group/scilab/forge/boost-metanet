// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================
//
//
function g = makeMetanetGraph (boostGraph)
    name = getGraphName (boostGraph);
    nodeNumber = getNodeNumber (boostGraph);
    heads = getHeadNodes (boostGraph);
	tails = getTailNodes (boostGraph);
	
	// Shift indices - Scilab counts from 1, C++ counts from 0
	heads = heads + 1;
	tails = tails + 1;
	
	g = make_graph(name, 0, nodeNumber, heads, tails);
	g('node_name') = getAllNodeLabels (boostGraph);
	g('edge_name') = getAllEdgeLabels (boostGraph);
	g('edge_length') = getAllEdgeWeights (boostGraph);

endfunction
// ====================================================================
