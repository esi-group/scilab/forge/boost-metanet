// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================
//
//
function p = findRedBlackOrdering (boostGraph)
    [d dt pred] = breadthFirstSearch(boostGraph, 0);
    d_even = find(pmodulo(d,2) == 0);
	d_odd = find(pmodulo(d,2) == 1);
    p = [d_odd' d_even'];
	
endfunction
// ====================================================================
