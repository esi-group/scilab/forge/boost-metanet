// ====================================================================
// Copyright INRIA 2008
// Allan CORNET
// This file is released into the public domain
// ====================================================================
mode(-1);
lines(0);

disp("Creating sample graph");
g = makeGraph("testGraph");

addNode(g, "node1");
addNode(g, "node2");
addNode(g, "node3");
addNode(g, "node4");

addEdge(g, 0, 1);
addEdge(g, "node2", "node3");
addEdge(g, 1, "node4");

setNodeLabel(g,"node1","newNode1");

printGraph(g);
printVertex(g, 1);
printVertex(g, "node2");
// ====================================================================
