//
// builder_src.sce --
//   Builder for the OO Scilab Toolbox
//

tbx_builder_src_lang('bglgraph', get_absolute_file_path('builder_src.sce'));
tbx_builder_src_lang('iohelper', get_absolute_file_path('builder_src.sce'));

clear tbx_builder_src_lang;
clear src_dir;
