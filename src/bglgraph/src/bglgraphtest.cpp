#include "../includes/bglgraph.hpp"

class BglGraphTest {
public:
	BglGraphTest() {
		graphName = "test graph";
		Setup();
	}
	~BglGraphTest() {
		TearDown();
	}
	void Setup() {
		g = BglGraph::CreateGraph(graphName);
		// add nodes etc.
	}

	void TearDown() {
		BglGraph::ClearAllGraphs();
	}

	void testCreateGraph() {
		size_t currentCount = BglGraph::GetGraphCount();
		BglGraph* newGraph = BglGraph::CreateGraph("testCreateGraph test graph");
		assert(newGraph->getNumVertices() == 0);
		assert(newGraph->getNumEdges() == 0);
		assert(BglGraph::GetGraphCount() == currentCount + 1);
	}

	void testGetGraph() {
		// we're assuming that Setup made at least one graph
		BglGraph* testGraphReference = BglGraph::GetGraph(graphName);
		assert(testGraphReference != NULL);
		assert(testGraphReference == g);
		assert(BglGraph::GetGraphCount() > 0);
	}
	void testRemoveGraph() {
		const string tempGraphName = "testRemoveGraph test graph";
		size_t currentCount = BglGraph::GetGraphCount();
		g = BglGraph::CreateGraph(tempGraphName);
		assert(BglGraph::GetGraphCount() == currentCount + 1);
		BglGraph::RemoveGraph(tempGraphName);
		assert(BglGraph::GetGraphCount() == currentCount);
		BglGraph* shouldBeNull = BglGraph::GetGraph(tempGraphName);
		assert(shouldBeNull == NULL); // graph has been properly removed
	}
	void testClearAllGraphs() {
		if (BglGraph::GetGraphCount() <= 0) {
			BglGraph* temp = BglGraph::CreateGraph("testClearAllGraphs test graph");
			assert(temp != NULL);
			assert(BglGraph::GetGraphCount() == 1);
		}

		// test clear
		BglGraph::ClearAllGraphs();
		assert(BglGraph::GetGraphCount() == 0);
	}

	void testAddNode() {
		// empty graph
		BglGraph* tempGraph = BglGraph::CreateGraph("testAddNode test graph");
		assert(tempGraph->getNumVertices() == 0);
		Vertex tnode1 = tempGraph->addNode("node 1");
		assert(tempGraph->getNumVertices() == 1);
		assert (tnode1 != (Vertex)-1);
		assert(tempGraph->getNodeLabel(tnode1) == "node 1");

		// non-empty graph
		size_t currentSize = g->getNumVertices();
		Vertex node1 = g->addNode("testAddNode node 1");
		assert(g->getNumVertices() == currentSize + 1);
		assert (node1 != (Vertex)-1);
		assert(g->getNodeLabel(node1) == "testAddNode node 1");
	}

	void testGetNode() {
		// empty graph
		BglGraph* tempGraph = BglGraph::CreateGraph("testGetNode test graph");
		assert(tempGraph->getNumVertices() == 0);
		Vertex tnode = tempGraph->addNode("testGetNode node 1");
		assert(tempGraph->getNumVertices() == 1);
		assert (tnode != (Vertex)-1);

		Vertex tnode1 = tempGraph->getNode("testGetNode node 1");
		assert (tnode == tnode1);
		Vertex tnode2 = tempGraph->getNode(0);
		assert (tnode == tnode2);

		// non-empty graph
		size_t currentNodeCount = g->getNumVertices();

		Vertex node = g->addNode("testGetNode node 1");
		assert(g->getNumVertices() == currentNodeCount + 1);
		assert (node != (Vertex)-1);

		Vertex node1 = g->getNode("testGetNode node 1");
		assert (node == node1);
		Vertex node2 = g->getNode(currentNodeCount);
		assert (node == node2);
	}

	void testEdge() {

		// empty graph
		BglGraph* tempGraph = BglGraph::CreateGraph("testAddEdge test graph");
		assert(tempGraph->getNumVertices() == 0);
		assert(tempGraph->getNumEdges() == 0);
		tempGraph->addNode("node 1");
		assert(tempGraph->getNumVertices() == 1);
		tempGraph->addNode("node 2");
		assert(tempGraph->getNumVertices() == 2);
		tempGraph->addEdge(0, 1, 3.75, "edge 1");
		assert(tempGraph->getNumEdges() == 1);
		Edge edge1 = tempGraph->getEdge("edge 1");
		assert( edge1 != Edge());
		assert(tempGraph->getEdgeWeight(edge1));
		assert(tempGraph->getEdgeLabel(edge1) == "edge 1");

		// non-empty graph
		size_t currentEdgeCount = g->getNumEdges();
		size_t currentNodeCount = g->getNumVertices();
		Vertex v1 = g->addNode("testAddEdge node 1");
		assert(g->getNumVertices() == currentNodeCount + 1);
		Vertex v2 = g->addNode("testAddEdge node 2");
		assert(g->getNumVertices() == currentNodeCount + 2);
		Edge newEdge = g->addEdge(v1, v2, 1.234, "testAddEdge edge 1");
		assert(g->getNumEdges() == currentEdgeCount + 1);

	}

	void testSetNodeLabel() {
		BglGraph* tempGraph = BglGraph::CreateGraph("testSetNodeLabel test graph");
		assert(tempGraph->getNumVertices() == 0);
		assert(tempGraph->getNumEdges() == 0);
		Vertex node = tempGraph->addNode("node 1");
		assert(tempGraph->getNodeLabel(node) == "node 1");
		tempGraph->setNodeLabel(node, "new label for node 1");
		assert(tempGraph->getNodeLabel(node) == "new label for node 1");
	}

	static BglGraph* createGraphForDfs() {
		BglGraph* testGraph = BglGraph::CreateGraph("DFS test graph");
		enum { a, b, c, d, e, f, g, h, i, N};
		char const* labels = "abcdefghi";
		for(size_t i = 0; i < N; i++) {
			testGraph->addNode(labels[i]);
		}
		testGraph->addEdge(a,b);
		testGraph->addEdge(a,e);
		testGraph->addEdge(a,d);
		testGraph->addEdge(b,e);
		testGraph->addEdge(d,e);
		testGraph->addEdge(c,e);
		testGraph->addEdge(c,f);
		testGraph->addEdge(e,f);
		testGraph->addEdge(g,h);
		testGraph->addEdge(h,i);
		testGraph->addEdge(g,i);

#if DEBUG
		cerr << " Created graph for DFS: " << endl;
#endif
		return testGraph;
	}
	static BglGraph* createGraphForBfs() {
		enum { r, s, t, u, v, w, x, y, N };
		const char* labels = "rstuvwxy";

		BglGraph* testGraph = BglGraph::CreateGraph("BFS test graph");

		for (size_t i = 0; i < N; i++) {
			testGraph->addNode(labels[i]);
		}

		testGraph->addEdge(r,s);
		testGraph->addEdge(s,w);
		testGraph->addEdge(w,t);
		testGraph->addEdge(w,x);
		testGraph->addEdge(t,x);
		testGraph->addEdge(t,u);
		testGraph->addEdge(u,y);
		testGraph->addEdge(x,y);
		testGraph->addEdge(r,v);
		return testGraph;
#if DEBUG
		cerr << " Created graph for BFS: " << endl;
#endif
	}

	void testBfs() {
		BglGraph* testGraph = BglGraphTest::createGraphForBfs();
		size_t num_nodes = testGraph->getNumVertices();
		vector<	Size > distances(num_nodes);
		vector<Size> times(num_nodes);
		vector<Vertex> parents(num_nodes);

		Size expected_distances[] = { 0, 1, 3, 4, 1, 2, 3, 4 };
		Vertex expected_parents[] = { 0, 0, 5, 2, 0, 1, 5, 6 };
		Size expected_times[] = { 1, 2, 5, 7, 3, 4, 6, 8 };

		testGraph->bfs(testGraph->getNode("r"), distances, times, parents);

		assert(distances == vector<Size>(expected_distances, expected_distances + num_nodes));
		assert(parents == vector<Vertex>(expected_parents, expected_parents + num_nodes));
		assert(times == vector<Size>(expected_times, expected_times + num_nodes));
	}

	void testDfs() {
		BglGraph* testGraph = BglGraphTest::createGraphForDfs();
		size_t num_nodes = testGraph->getNumVertices();
		vector<Size> distances(num_nodes);
		vector<Size> discovery_times(num_nodes), finish_times(num_nodes);
		vector<Vertex> parents(num_nodes);

		Size expected_distances[] = { 0, 1, 3, 3, 2, 4, 0, 1, 2 };
		Vertex expected_parents[] = { 0, 0, 4, 4, 1, 2, 0, 6, 7 };
		Size expected_discovery_times[] = {1, 2, 5, 4, 3, 6, 7, 8, 9 };
		Size expected_finish_times[] = {6, 5, 3, 1, 4, 2, 9, 8, 7 };

		testGraph->dfs(distances, discovery_times, finish_times, parents);

		assert(distances == vector<Size>(expected_distances, expected_distances + num_nodes));
		assert(discovery_times == vector<Size>(expected_discovery_times, expected_discovery_times + num_nodes));
		assert(finish_times == vector<Size>(expected_finish_times, expected_finish_times + num_nodes));
		assert(parents == vector<Vertex>(expected_parents, expected_parents + num_nodes));
	}

	void testDijkstra() {
		BglGraph* testGraph = BglGraph::GetGraph("BFS test graph");
		size_t num_nodes = testGraph->getNumVertices();
		vector<	Size > distances(num_nodes);
		vector<Vertex> parents(num_nodes);

		Size expected_distances[] = { 0, 1, 3, 4, 1, 2, 3, 4 };
		Vertex expected_parents[] = { 0, 0, 5, 2, 0, 1, 5, 6 };

		testGraph->dijkstra(testGraph->getNode("r"), distances, parents);

		assert(distances == vector<Size>(expected_distances, expected_distances + num_nodes));
		assert(parents == vector<Vertex>(expected_parents, expected_parents + num_nodes));
	}
private:
	BglGraph* g;
	string graphName;
};
int main() {
	BglGraphTest test;
	test.testCreateGraph();
	test.testGetGraph();
	test.testRemoveGraph();
	test.testAddNode();
	test.testEdge();
	test.testGetNode();
	test.testSetNodeLabel();
	test.testBfs();
	test.testDfs();
	test.testDijkstra();
	cout << "All tests passed" << endl;

}
