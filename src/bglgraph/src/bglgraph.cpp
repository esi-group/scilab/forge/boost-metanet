#include "../includes/bglgraph.hpp"
#include <iostream>
#include <fstream>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/bellman_ford_shortest_paths.hpp>

map<string, BglGraph*> BglGraph::_allGraphs;
size_t BglGraph::_graphCount = 0;

BglGraph* BglGraph::GetGraph(const string & name) {
	if ( _allGraphs.find(name) != _allGraphs.end())
		return _allGraphs[name];
	else return NULL;
}

void BglGraph::ClearAllGraphs() {
	for(map<string, BglGraph*>::iterator it = _allGraphs.begin(); it != _allGraphs.end(); it++)
		delete it->second;
	_allGraphs.clear();
}

BglGraph* BglGraph::CreateGraph(const string & name) {
	if ( _allGraphs.find(name) != _allGraphs.end()) {
#if DEBUG
		cerr << "CreateGraph(): error: Graph " << name << " already exists." << endl;
#endif
		return NULL;
	}
	BglGraph* newGraph = new BglGraph(name);
	_allGraphs[name] = newGraph;
	return newGraph;
}

bool BglGraph::RemoveGraph(const string & name) {
	map<string, BglGraph*>::iterator iFound = _allGraphs.find(name);
	if (iFound == _allGraphs.end()) {
#if DEBUG
		cerr << "RemoveGraph(): error: Graph " << name << " not found." << endl;
#endif
		return false;
	}
	delete iFound->second;
	_allGraphs.erase(iFound);
	return true;
}

size_t BglGraph::GetGraphCount() {
	return BglGraph::_graphCount;
}

BglGraph::BglGraph(const string & name) {
	_graphCount++;
	_name = name;
#if DEBUG
	cerr << "Made graph with name " << _name << endl;
#endif
	_node_labels = get(&VertexProperties::label, _graph);
	_node_indices = get(vertex_index, _graph);
	//	_node_weights = get(&VertexProperties::weight, _graph);

	_edge_weights = get(edge_weight, _graph);
	_edge_labels = get(&EdgeProperties::label, _graph);
}

BglGraph::~BglGraph() {
	_graphCount--;
}

Vertex BglGraph::addNode(char charLabel) {
	return addNode(string(1, charLabel));
}

Vertex BglGraph::addNode(string label) {
	Vertex new_vertex = add_vertex(_graph);
	get(&VertexProperties::label, _graph)[new_vertex] = label;

#if DEBUG
	cerr << "Added vertex number " << _node_indices[new_vertex] << " with label " << _node_labels[new_vertex] << endl;
#endif
	return new_vertex;
}

Vertex BglGraph::getNode(string label) {

	VertexIter vi, vi_end;
	_node_labels = get(&VertexProperties::label, _graph);
	for (boost::tie(vi,vi_end) = vertices(_graph); vi != vi_end; vi++) {
		if (_node_labels[*vi] == label) {
#if DEBUG
			cerr << "Found node with label " << label << endl;
#endif
			return *vi;
		}
	}
#if DEBUG
	cerr << "Vertex with label " << label << " not found" << endl;
#endif

	return INVALID_VERTEX;
}

Vertex BglGraph::getNode(size_t index) {

	VertexIter vi, vi_end;
	_node_indices = get(vertex_index, _graph);
	for (boost::tie(vi,vi_end) = vertices(_graph); vi != vi_end; vi++) {
		if (_node_indices[*vi] == index) {
#if DEBUG
			cerr << "Found node with index " << index << endl;
#endif
			return *vi;
		}
	}
#if DEBUG
	cerr << "Vertex with index " << index << " not found" << endl;
#endif
	return INVALID_VERTEX; //error
}
Edge BglGraph::getEdge(string label) {
	EdgeIter ei, ei_end;
	_edge_labels = get(&EdgeProperties::label, _graph);
	for (tie(ei, ei_end) = edges(_graph); ei != ei_end; ei++) {
		if (_edge_labels[*ei] == label) {
#if DEBUG
			cerr << "Found edge with label " << label << endl;
#endif
			return *ei;
		}
	}
#if DEBUG
	cerr << "Edge with label " << label << " not found!" << endl;
#endif
	return Edge();
}

Edge BglGraph::getEdge(Vertex v1, Vertex v2) {
	graph_traits<Graph>::adjacency_iterator ai, ai_end;
	for (tie(ai, ai_end) = adjacent_vertices(v1, _graph); ai != ai_end; ai++) {
		if (*ai == v2) {
#if DEBUG
			cerr << "Found edge between " << v1 << " " << " and " << v2 << endl;
#endif
			std::pair<Edge, bool> foundEdge = edge(v1, v2, _graph);
			if (foundEdge.second)
				return foundEdge.first;

		}
	}
#if DEBUG
	cerr << "Edge with index " << index << " not found!" << endl;
#endif
	return Edge();
}

string BglGraph::getNodeLabel(Vertex node) {
	if (VERTEX_VALID(vertex(node, _graph)))
		return _node_labels[node];
#if DEBUG
	cerr << "No node found" << endl;
#endif
	return "";
}
string BglGraph::getEdgeLabel(Edge edge){
	if (edge != Edge())
		return get(&EdgeProperties::label, _graph)[edge];
#if DEBUG
	cerr << "No node found" << endl;
#endif
	return "";
}
double BglGraph::getEdgeWeight(Edge edge) {
	if (edge != Edge())
		return get(edge_weight, _graph)[edge];
#if DEBUG
	cerr << "Edge not found" << endl;
#endif
	return 1.0;
}

vector<Vertex> BglGraph::getHeadNodes() const {
    vector<Vertex> nodes;
    EdgeIter ei, ei_end;
    
    tie(ei, ei_end) = edges(_graph);
    nodes.reserve(num_edges(_graph));
    
    for (; ei != ei_end; ei++)
        nodes.push_back(source(*ei, _graph));
    return nodes;
}
vector<Vertex> BglGraph::getTailNodes() const {
    vector<Vertex> nodes;
    EdgeIter ei, ei_end;
    
    tie(ei, ei_end) = edges(_graph);
    nodes.reserve(num_edges(_graph));
    
    for (; ei != ei_end; ei++)
        nodes.push_back(target(*ei, _graph));
    return nodes;
}

vector<string> BglGraph::getAllNodeLabels() const {
    vector<string> labels;
    labels.reserve(num_vertices(_graph));
    VertexIter vi, vi_end;
    for (tie(vi, vi_end) = vertices(_graph); vi != vi_end; vi++)
        labels.push_back(get(&VertexProperties::label, _graph)[*vi]);
    return labels;
}

vector<string> BglGraph::getAllEdgeLabels() const {
    vector<string> labels;
    EdgeIter ei, ei_end;
    
    tie(ei, ei_end) = edges(_graph);
    labels.reserve(num_edges(_graph));
    
    for (; ei != ei_end; ei++)
        labels.push_back(get(&EdgeProperties::label, _graph)[*ei]);
    return labels;
}

vector<double> BglGraph::getAllEdgeWeights() const  {
    vector<double> weights;
    EdgeIter ei, ei_end;
    
    tie(ei, ei_end) = edges(_graph);
    weights.reserve(num_edges(_graph));
    
    for (; ei != ei_end; ei++)
        weights.push_back(get(edge_weight, _graph)[*ei]);
    return weights;
}

void BglGraph::setEdgeWeight(Edge edge, double weight) {
	if (edge != Edge())
		get(edge_weight,_graph)[edge] = weight;
}

void BglGraph::setNodeLabel(Vertex node, string label) {
	if (VERTEX_VALID(node))
		get(&VertexProperties::label, _graph)[node] = label;
	else
#if DEBUG
		cerr << "Node not found" << endl;
#endif
	;
}
void BglGraph::setEdgeLabel(Edge edge, string label) {
	if (edge != Edge())
		get(&EdgeProperties::label, _graph)[edge] = label;
}

Edge BglGraph::addEdge(Vertex v1, Vertex v2, double edgeWeight, string label) {
	pair<Edge, bool> newEdge = add_edge(v1, v2, edgeWeight, _graph);
	_edge_labels[newEdge.first] = label;
	return newEdge.first;
}

void BglGraph::printGraphToStream(ostream& ostr, bool full) {
	VertexIter vi, vi_end;
	_node_indices = get(vertex_index, _graph);
	_node_labels = get(&VertexProperties::label, _graph);
	graph_traits<Graph>::out_edge_iterator ei, ei_end;

	//for every vertex...
	for (tie(vi,vi_end) = vertices(_graph); vi != vi_end; vi++) {
		string label = _node_labels[*vi];
		if (label.length() > 0)
			ostr << label;
		else
			ostr << _node_indices[*vi];

		ostr << ": ";
		// and for all vertices connected to it...
		for (boost::tie(ei,ei_end) = out_edges(*vi, _graph); ei != ei_end; ++ei) {
			ostr  << " ";
			string label = get(&VertexProperties::label, _graph)[target(*ei, _graph)];
			if (label.length())
				ostr << label;
			else
				ostr <<  _node_indices[target(*ei, _graph)];
		}
		ostr << std::endl;
	}
	if (full) {
		ostr << string(25,'-') << endl;
		ostr << "All nodes: " << endl;
		for (tie(vi, vi_end) = vertices(_graph); vi != vi_end; vi++)
			printVertex(*vi, ostr);

		ostr << string(25,'-')  << endl;
		ostr << "All edges: " << endl;
		EdgeIter ei1, ei_end1;
		tie(ei1, ei_end1) = edges(_graph);
		for (; ei1 != ei_end1; ei1++)
			printEdge(*ei1, ostr);
	}
}

void BglGraph::printVertex(Vertex vertex, ostream& ostr) const {
	if (vertex == INVALID_VERTEX)
		return;
	string label = get(&VertexProperties::label, _graph)[vertex];
	if (label.length() > 0)
		ostr << "Label: " << label << " ";
	ostr << "Index: " << get(vertex_index, _graph)[vertex] << endl;
}

void BglGraph::printEdge(Edge edge, ostream& ostr) const {
	ostr << edge.m_source << " - " << edge.m_target << endl;
	if ( (get(&EdgeProperties::label, _graph)[edge]).length())
		ostr << "Label: " << get(&EdgeProperties::label, _graph)[edge].c_str() << endl;
	ostr << "Weight: " << get(edge_weight, _graph)[edge] << endl;
}

void BglGraph::dijkstra(Vertex start, vector<double> & distances, vector<Vertex>& parents) {
	fill(distances.begin(), distances.end(), 0);
	fill(parents.begin(), parents.end(), INVALID_VERTEX);
	parents[vertex(start, _graph)] = vertex(start, _graph);
	dijkstra_shortest_paths(_graph, vertex(start, _graph),
			predecessor_map(make_iterator_property_map(parents.begin(), get(vertex_index, _graph))).weight_map(get(edge_weight, _graph)).distance_map(make_iterator_property_map(distances.begin(), get(vertex_index, _graph))));
}

void BglGraph::bfs(Vertex start, vector<double>& distances, vector<Size>& discovery_times, vector<Vertex>& parents) {
	fill(distances.begin(), distances.end(), 0);
	fill(discovery_times.begin(), discovery_times.end(), 0);
	fill(parents.begin(), parents.end(), INVALID_VERTEX);

	Size time = 0;
	NodeIndices vertex_id = get(vertex_index, _graph);
	parents[vertex(start,_graph)] = vertex(start,_graph);

	breadth_first_search(_graph, vertex(start, _graph),
			visitor(boost::make_bfs_visitor(
					make_pair(record_distances(&distances[0], boost::on_tree_edge()),
							make_pair(
									record_predecessors(&parents[0], boost::on_tree_edge()),
									stamp_times(make_iterator_property_map(discovery_times.begin(), get(vertex_index, _graph), time), time, boost::on_discover_vertex())
							))
			)
			)
	);
}

void BglGraph::dfs(vector<double>& distances, vector<size_t>& discovery_times, vector<Size>& finish_times, vector<Vertex>& parents) {
	fill(distances.begin(), distances.end(), 0);
	fill(discovery_times.begin(), discovery_times.end(), 0);
	fill(finish_times.begin(), finish_times.end(), 0);
	fill(parents.begin(), parents.end(), Vertex());
	Size start_time = 0;
	Size finish_time = 0;
	NodeIndices vertex_id = get(vertex_index, _graph);

	depth_first_search(_graph, visitor(boost::make_dfs_visitor(
			make_pair(boost::record_distances(&distances[0], boost::on_tree_edge()),
					make_pair(
							record_predecessors(&parents[0], boost::on_tree_edge()),
							make_pair(
									stamp_times(make_iterator_property_map(discovery_times.begin(), get(vertex_index, _graph), start_time), start_time, boost::on_discover_vertex()),
									stamp_times(make_iterator_property_map(finish_times.begin(), get(vertex_index, _graph), finish_time), finish_time, boost::on_finish_vertex())
							)

					))
	)
	));
}

void BglGraph::bellman_ford(Vertex start, vector<double> & distances, vector<Vertex>& parents) {
	fill(distances.begin(), distances.end(), 0);
	fill(parents.begin(), parents.end(), INVALID_VERTEX);
	distances[get(vertex_index, _graph)[start] ] = 0;
	parents[vertex(start, _graph)] = vertex(start, _graph);
	bool negativeCycle = bellman_ford_shortest_paths(_graph, (int)num_vertices(_graph),
			predecessor_map(make_iterator_property_map(parents.begin(), get(vertex_index, _graph)))
			.weight_map(get(edge_weight, _graph))
			.distance_map(make_iterator_property_map(distances.begin(), get(vertex_index, _graph))));

	if (negativeCycle) {
		cout << "There is a negative cycle in the graph. There are edges in the graph that were not properly minimized." << endl;
		distances.clear();
		parents.clear();
	}
}
