#ifndef __BGL_GRAPH_HPP__
#define __BGL_GRAPH_HPP__

#include <boost/graph/adjacency_list.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/utility.hpp>

// general libraries
#include <algorithm>
#include <utility>
#include <iostream>
#include <vector>
#include <string>

// for BFS/DFS
#include <boost/graph/visitors.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/graph_utility.hpp>

using namespace std;
using namespace boost;

//#define DEBUG 1

#define INVALID_VERTEX (Vertex)(-1)
#define VERTEX_VALID(v) (v) != INVALID_VERTEX

struct VertexProperties {
	string label;
	//	double weight;
	vector<double> coordinates;
};

struct EdgeProperties {
	string label;
	//	double weight;
};

typedef adjacency_list<vecS, vecS, undirectedS, VertexProperties,
property<edge_weight_t, double, EdgeProperties> > Graph;

typedef property_map<Graph, string VertexProperties::*>::type NodeLabels;
typedef property_map<Graph, vertex_index_t>::type NodeIndices;
//typedef property_map<graph_t, double VertexProperties::*>::type node_weight_map_t;

typedef property_map<Graph, string EdgeProperties::*>::type EdgeLabels;
typedef property_map<Graph, edge_weight_t>::type EdgeWeights;

typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef graph_traits<Graph>::vertex_iterator VertexIter;

typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::edge_iterator EdgeIter;

typedef graph_traits < Graph >::vertices_size_type Size;

class BglGraph {

public:
	static BglGraph* GetGraph(const string & name);
	static void ClearAllGraphs();
	static BglGraph* CreateGraph(const string & name);
	static bool RemoveGraph(const string & name);
	static size_t GetGraphCount();

	Vertex addNode(string label);
	Vertex addNode(char label);
	Edge addEdge(Vertex, Vertex, double edgeWeight = 1.0, string label = "");

	string getName() const { return _name; }
	Vertex getNode(string label);
	Vertex getNode(size_t index);
	Edge getEdge(string label);
	Edge getEdge(Vertex v1, Vertex v2);

	string getNodeLabel(Vertex node);
	string getEdgeLabel(Edge edge);
	double getEdgeWeight(Edge edge);

    vector<Vertex> getHeadNodes() const;
    vector<Vertex> getTailNodes() const;
    vector<string> getAllNodeLabels() const;
    vector<string> getAllEdgeLabels() const;
    vector<double> getAllEdgeWeights() const;
    
	void setNodeLabel(Vertex node, string label);
	void setEdgeLabel(Edge edge, string label);
	void setEdgeWeight(Edge edge, double weight);

	void printGraphToStream(ostream& ostr, bool full = false);
	void printVertex(Vertex vertex, ostream& ostr) const;
	void printEdge(Edge edge, ostream& ostr) const;

	size_t getNumVertices() {
		return num_vertices(_graph);
	}

	size_t getNumEdges() {
		return num_edges(_graph);
	}

	void bfs(Vertex start, vector<double>& distances, vector<Size>& discovery_times, vector<Vertex>& parents);
	void dfs(vector<double>& distances, vector<size_t>& discovery_times, vector<size_t>& finish_times, vector<Vertex>& parents);
	void dijkstra(Vertex start, vector<double> & distances, vector<Vertex>& parents);
	void bellman_ford(Vertex start, vector<double> & distances, vector<Vertex>& parents);
private:
	static map<string, BglGraph*> _allGraphs;
	static size_t _graphCount;

	BglGraph(const string & name) ;
	~BglGraph();
	Graph _graph;
	string _name;

	NodeLabels _node_labels;
	NodeIndices _node_indices;

	EdgeWeights _edge_weights;
	EdgeLabels _edge_labels;

};

#endif
